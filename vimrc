"        .__
"  ___  _|__| ____________   ____
"  \  \/ /  |/     \_  __ \_/ ___\
"   \   /|  |  Y Y  \  | \/\  \___
" /\ \_/ |__|__|_|  /__|    \___  >
" \/              \/            \/
"
" Compatabilities
if !has('nvim')
	set ttymouse=xterm2
endif


" +--------+
" | Leader |
" +--------+
let mapleader='``'

" Pasting

set pastetoggle=<F10>
" CTRL-v copy and CTRL-v pasting
inoremap <C-V> <F10><C-r>+<F10>
vnoremap <C-C> "+y

" --------------
" Searching
" -------------

" +---------------+
" | Substitutions |
" +---------------+
" Substitute in selection
vnoremap <leader>s :s///g


" +--------------------------+
" | Window & Buffer controls |
" +--------------------------+
" +-- Window --+
map <leader>w <C-w>
map <leader><Tab> <C-w>
" split controls
nmap <leader>- <C-w><C-n>
nmap <leader>/ <C-w><C-v>
map <C-w>- <C-W><C-S>
map <C-w>/ <C-W><C-V>
" window movement
nmap <leader>j <C-w>j
nmap <leader>k <C-w>k
nmap <leader>h <C-w>h
nmap <leader>l <C-w>l
" closing windows
nmap <leader>q <C-w>q
" tab controls
noremap <C-w>t <C-W>T
map <C-w>] gt
map <C-w>[ gT
"+-- Buffer --+
" open buffer [N] +/- 1
map <leader>b :sb<CR>
map <leader>n :bn<CR>

" Big brain Abbreviations
iabbrev ammount amount
iabbrev gaurd guard


" +---------+
" | Plugins |
" +---------+
"
" Pathogen
execute pathogen#infect()

" +- Nerdtree -+
map <leader>; :NERDTreeToggle<CR>

" +- cht.sh -+
" cheat sheet pager
let g:CheatPager='less -R'


" +---------+
" | Display |
" +---------+
" +-- Help --+
" Enable syntax highlighting
set filetype?
syntax on
" Show a red line on the 80th text column.
set colorcolumn=80
" Numbering
set number relativenumber
augroup numbertoggle
  " Set relative number in anything but insert/replace mode
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
" Better wrapping
"set breakindent
"set showbreak=

" +-------------------------+
" | Filetype Command Groups |
" +-------------------------+

augroup yaml
	autocmd!
	au BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml

	au FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
augroup END

augroup xml
	autocmd!
	au BufNewFile,BufReadPost *.xml set filetype=xml

	au FileType xml setlocal ts=2 sts=2 sw=2 expandtab

augroup END


augroup html
	autocmd!
	au FileType html setlocal ts=2 sts=2 sw=2 expandtab

augroup END

augroup sh
	autocmd!
	au BufNewFile,BufFilePre,BufRead *.sh set filetype=sh
	" detect shebang
	if getline(1) == '#!/bin/sh'
        	set filetype=sh
    	endif
	if getline(1) == '#!/usr/bin/env bash'
        	set filetype=sh
    	endif
	" Handy Bash abbreviations
	"abbrev #!date $(date + %F)
	"abbrev #!datetime '$(date + %F_%R)
	" Enable syntax highlighting (coloring)

augroup END

augroup markdown
	autocmd!
	au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown

	" Markdown match groups
	highlight MarkdownTrailingSpaces ctermbg=green guibg=green

	" Match group parameters
	syntax match MarkdownTrailingSpaces "\s{2,}"

	" Abbreviations
	"               <-10chrs->  <-10chrs->
	" iabbrev @@img ![<alt-text>](<img-link>)
	" iabbrev @@lnk  [<link-txt>](<hypr-lnk>)
augroup END
" +--------+
" | Python |
" +--------+
augroup python_pep8
" Python writing autocommands
" Follows from the PEP8 Python Style Guide
" https://www.python.org/dev/peps/pep-0008

	autocmd!
	au FileType python set omnifunc=pythoncomplete#Complete


	" +---- Linting ----+
	"" Python writing autocommands
	" Follows from the PEP8 Python Style Guide
	" https://www.python.org/dev/peps/pep-0008
	" Code should not go beyond 79 characters
	" Comments should not go beyond 72 characters
	au Filetype python set colorcolumn=72,79
	"	au FileType python set foldmethod=indent

	" Indentation settings
	au FileType python set tabstop=4
	au FileType python set shiftwidth=4
	au FileType python set softtabstop=4
	au FileType python set expandtab

	" Code should not go beyond 79 characters
	" Comments should not go beyond 72 characters
	au Filetype python set colorcolumn=72,79
	"	au FileType python set foldmethod=indent

		" clear trailing whitespace
	autocmd BufWritePre * %s/\s\+$//e

	" pylint
	set makeprg=pylint\ --reports=n\ --msg-template=\"{path}:{line}:\ {msg_id}\ {symbol},\ {obj}\ {msg}\"\ %:p
	au BufWritePost *.py make

	" Check out retab!

augroup END

